import string
specialChar = set(string.punctuation)
def charaTypeCounter():
    str = input("Enter the string: ")
    upperC = 0
    lowerC = 0
    numericC= 0
    specialC = 0
    for i in str:
        if(i.isupper()):
            upperC+=1
        elif(i.islower()):
            lowerC+=1
        elif(i.isnumeric()):
            numericC+=1
        elif(i in specialChar):
            specialC+=1
    print("No.of Uppercase characters: ",upperC)
    print("No.of Lowercase characters: ",lowerC)
    print("No.of Numeric characters: ",numericC)
    print("No.of Special characters: ",specialC)
charaTypeCounter()

