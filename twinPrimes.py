def prime(n):
   for i in range(2, n):
      if n % i == 0:
         return False
   return True

def twinPrimes(end):
   for i in range(2, end):
      j = i + 2
      if(prime(i) and prime(j)):
         print("[{:d},{:d}]".format(i, j))

twinPrimes(1000)
